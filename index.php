<?php

include_once("config.php");


$result = mysqli_query($mysqli, "SELECT * FROM tb_datadiri ORDER BY id DESC");
?>

<html>

<head>
    <title>Data Diri</title>
</head>

<body>
    <a href="add.php">Tambah Data Diri</a><br /><br />

    <table width='80%' border=1>

        <tr>
            <th>Nama</th>
            <th>Umur</th>
            <th>Kota</th>
            <th>Update</th>
        </tr>
        <?php
        while ($echo = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>" . $echo['nama'] . "</td>";
            echo "<td>" . $echo['umur'] . "</td>";
            echo "<td>" . $echo['kota'] . "</td>";
            echo "<td><a href='edit.php?id=$echo[id]'>Edit</a> | <a href='delete.php?id=$echo[id]'>Delete</a></td></tr>";
        }
        ?>
    </table>
</body>

</html>